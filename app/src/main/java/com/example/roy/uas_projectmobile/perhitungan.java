package com.example.roy.uas_projectmobile;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class perhitungan extends AppCompatActivity {
    ListView listview1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perhitungan);
        listview1 = (ListView) findViewById(R.id.listSuhu);
        String[] values1 = new String[] {"Celsius ke Fahrenheit","Fahrenheit ke Celsius", "Celsius ke Kelvin", "Kelvin ke Celsius"};
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>
                (this, android.R.layout.activity_list_item, android.R.id.text1, values1);
        listview1.setAdapter(adapter1);
        listview1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    Intent intent = new Intent(view.getContext(), fahrenheit.class);
                    startActivityForResult(intent, 0);
                }
                if (position==1) {
                    Intent intent = new Intent(view.getContext(), celsius.class);
                    startActivityForResult(intent, 1);
                }
                if (position==2) {
                    Intent intent = new Intent(view.getContext(), kelvin.class);
                    startActivityForResult(intent, 2);
                }
                if (position==3) {
                    Intent intent = new Intent(view.getContext(), suhu1.class);
                    startActivityForResult(intent, 3);
                }
            }
        });
    }
}
