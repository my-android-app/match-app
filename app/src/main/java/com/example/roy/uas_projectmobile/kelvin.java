package com.example.roy.uas_projectmobile;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class kelvin extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kelvin);
    }
    public void kelvin(View v) {
        EditText k = (EditText) findViewById(R.id.etKelvin);
        TextView hasil = (TextView) findViewById(R.id.tvHasilkelvin);

        String angka1 = k.getText().toString();

        double k1 = Double.parseDouble(angka1);
        double k2 = k1+273.15;

        String  hasilKelvin= String.valueOf(k2);
        hasil.setText(hasilKelvin+" K");
    }}