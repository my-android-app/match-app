package com.example.roy.uas_projectmobile;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class celsius extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_celsius);
    }
    public void celsius(View v) {
        EditText c = (EditText) findViewById(R.id.etCelsius);
        TextView hasil = (TextView) findViewById(R.id.tvHasilcelsius);

        String angka1 = c.getText().toString();

        double c1 = Double.parseDouble(angka1);
        double c2 = (c1-32)/1.8;

        String  hasilCelsius= String.valueOf(c2);
        hasil.setText(hasilCelsius+ " C");
    }}