package com.example.roy.uas_projectmobile;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class lingkaran extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lingkaran);
    }
    public void hasilLuas(View v) {
        EditText a = (EditText) findViewById(R.id.etluas);
        TextView luas = (TextView) findViewById(R.id.tvluas);

        String angka1 = a.getText().toString();

        double a1 = Double.parseDouble(angka1);

        if(a1%7 == 0){
            double a2 = 22*(a1*a1)/7;
            String  hasilLuas= String.valueOf(a2);
            luas.setText(hasilLuas);
        }else{
            double a2 = 3.14*(a1*a1);
            String  hasilLuas= String.valueOf(a2);
            luas.setText(hasilLuas);
        }
    }
    public void hasilvolume(View v) {
        EditText b = (EditText) findViewById(R.id.etvolume);
        TextView volume = (TextView) findViewById(R.id.tvvolume);

        String angka1 = b.getText().toString();

        double b1 = Double.parseDouble(angka1);

        if(b1%7 == 0){
            double b2 = (4/3)*22*(b1*b1)/7;
            String  hasilVolume= String.valueOf(b2);
            volume.setText(hasilVolume);
        }else{
            double b2 = (4/3)*3.14*(b1*b1);
            String  hasilVolume= String.valueOf(b2);
            volume.setText(hasilVolume);
        }
    }
}
