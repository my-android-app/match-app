package com.example.roy.uas_projectmobile;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class fahrenheit extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fahrenheit);
    }
    public void hasil(View v) {
        EditText a = (EditText) findViewById(R.id.etFahrenheit);
        TextView hasil1 = (TextView) findViewById(R.id.tvHasil);

        String angka1 = a.getText().toString();

        double a1 = Double.parseDouble(angka1);
        double a3 = (a1*1.8)+32;

        String hasilbagi = String.valueOf(a3);
        hasil1.setText(hasilbagi+" F");
    }}

