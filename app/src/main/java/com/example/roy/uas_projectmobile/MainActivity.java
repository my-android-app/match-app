package com.example.roy.uas_projectmobile;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {
ListView listview;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listview = (ListView) findViewById(R.id.listHasil);
        String[] values = new String[] {"Suhu", "Waktu", "lingkaran"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (this, android.R.layout.activity_list_item, android.R.id.text1, values);
        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    Intent intent = new Intent(view.getContext(), perhitungan.class);
                    startActivityForResult(intent, 0);
                }
                if (position==1) {
                    Intent intent = new Intent(view.getContext(), panjang.class);
                    startActivityForResult(intent, 1);
                }
                if (position==2) {
                    Intent intent = new Intent(view.getContext(), lingkaran.class);
                    startActivityForResult(intent, 2);
                }
            }
        });
    }
}
