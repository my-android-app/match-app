package com.example.roy.uas_projectmobile;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class panjang extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panjang);
    }
    public void hasilHari(View v) {
        EditText a = (EditText) findViewById(R.id.etHari);
        TextView detik = (TextView) findViewById(R.id.tvMenit);

        String angka1 = a.getText().toString();

        double a1 = Double.parseDouble(angka1);
        double a2 = a1*1440;

        String  hasilMenit= String.valueOf(a2);
        detik.setText(hasilMenit+" M");
    }
    public void hasilmenithari(View v) {
        EditText b = (EditText) findViewById(R.id.etmenitHari);
        TextView hari = (TextView) findViewById(R.id.tvHari);

        String angka1 = b.getText().toString();

        double b1 = Double.parseDouble(angka1);
        double b2 = 1440/b1;

        String  hasilHari= String.valueOf(b2);
        hari.setText(hasilHari+" Days");
    }
    public void hasilDetik(View v) {
        EditText c = (EditText) findViewById(R.id.ethariDetik);
        TextView detik = (TextView) findViewById(R.id.tvDetik);

        String angka1 = c.getText().toString();

        double c1 = Double.parseDouble(angka1);
        double c2 = (c1*86400);

        String  hasilDetik= String.valueOf(c2);
        detik.setText(hasilDetik+" Seconds");
    }
    public void hasildetikHari(View v) {
        EditText d = (EditText) findViewById(R.id.etdetikHari);
        TextView detikHari = (TextView) findViewById(R.id.tvDetikhari);

        String angka1 = d.getText().toString();

        double d1 = Double.parseDouble(angka1);
        double d2 = (86400/d1);

        String  hasilhariDetik= String.valueOf(d2);
        detikHari.setText(hasilhariDetik+" Days");
    }
}
