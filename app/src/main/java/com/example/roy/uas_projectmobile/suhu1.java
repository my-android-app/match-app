package com.example.roy.uas_projectmobile;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class suhu1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suhu1);
    }
    public void suhu(View v) {
        EditText s = (EditText) findViewById(R.id.etSuhu);
        TextView hasil = (TextView) findViewById(R.id.tvHasilsuhu);

        String angka1 = s.getText().toString();

        double s1 = Double.parseDouble(angka1);
        double s2 = s1-273.15;

        String  hasilSuhu= String.valueOf(s2);
        hasil.setText(hasilSuhu+" C");
    }}